import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
List <Rectangulo>rectangulos;
List <Rectangulo>tmpw;
Rectangulo c,c2,c3,c4,tmp;
Iterator it;
String str="";
int n=0;
int indice=0;
public void setup(){
   size(450,250);
   rectangulos = new ArrayList();
   c = new Rectangulo((int)(Math.random()*width),50,40,20,(int)(Math.random()*10+1));
   c2 = new Rectangulo((int)(Math.random()*width),90,35,20,(int)(Math.random()*10+1));
   c3 = new Rectangulo((int)(Math.random()*width),130,45,20,(int)(Math.random()*10+1));
   c4 = new Rectangulo((int)(Math.random()*width),170,45,20,(int)(Math.random()*10+1));
   rectangulos.add(c);
   rectangulos.add(c2);
   rectangulos.add(c3);
   rectangulos.add(c4);
   frameRate(10);
   it = rectangulos.iterator();
   tmpw = new ArrayList(rectangulos);
   
}

public  void draw(){
   background(128);
   rectangulos= new ArrayList(tmpw);
   it = rectangulos.iterator();
   while(it.hasNext()){
      tmp= (Rectangulo)it.next();
      tmp.dibujar();
      tmp.acelera();
   }
   text("Haz hecho click "+n+" veces",width/2-100,20);
   if (n>0)
   text("Haz clickeado el "+str,width/2-100,40);
}

public   void mousePressed(){
    float x = mouseX;
    float y = mouseY;
    it = rectangulos.iterator();
    while(it.hasNext()){
    tmp = (Rectangulo)it.next();
    if(tmp.detener(x,y)){
         str = ""+tmp.getNumero();
         indice = rectangulos.indexOf(tmp);
         str += " - indice=> "+indice;
           tmpw.remove(indice);
         
    }
    }
    n++;
}