
public class Rectangulo{
   PFont f;
   float x;
   float y;
   float ancho;
   float alto;
   float vel=0;
   boolean ban = false;
   int numero=0;
   public Rectangulo(float a, float b, float an, float al, float v){
         x = a;
         y = b;
         ancho = an;
         alto = al;
         vel = v;
         f = createFont("Arial",16,true);
         textFont(f,20);
         numero = (int)(Math.random()*10 );
   }
   public void dibujar(){
      fill(255);
      rect(x,y,ancho,alto);
      fill(0);
      text(""+numero,x+(ancho/2)-8,y+(alto/2)+8);
   }
   public void acelera(){
       if(!ban){
          x = x+ vel;
       }
       if( x > width){
         x = 0;
       }
   }
   public boolean detener(float p,float q){
        if ( p >= x && p <= (x+ancho) && q >= y && q <= (y+alto) ) {
            ban = !ban;
            return true;
        }
        else 
        return false;
   }
   
   public int getNumero(){
      return this.numero;
   }
}